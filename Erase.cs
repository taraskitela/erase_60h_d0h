﻿using NAND_Prog;
using NAND_Prog.UILib;
using System.ComponentModel.Composition;

namespace Erase_60h_D0h
{
    [Export(typeof(Operation)),
        ExportMetadata("Name", "Erase_60h_D0h")]
    public class Erase : AbstractErase
    {
        

        public Erase()
        {
            name = "Erase";                           // Це поле заповнює користувач ( в конструкторі чіпа)
            based = typeof(Block);

            optBox.AddOption(new VerifyBySR());         // результат виконання цієї операції можна(це опція) перевіряти на стороні самого чіпа , первіряючи статус-регістр
                                                        // Це поле заповнює користувач (вибираючи в конструкторі чіпа відповідний чек-бокс)
                                                        // Але це тільки можливість (Тобто в модальному вікні , яке появляється перед виконанням операції 
                                                        // , появится чек-бокс який буде активний) 
                                                        //Чи її застосувати вирішується на етапі виконання операції BlockErase
                                                        //При цьому повинен бути реалізований статус- регістр

            optBox.AddOption(new IgnoreBadStatus());    // По замовчуванню , операція стирання обходить бед блоки 
                                                        // Але при потребі можна стерти все


            Instruction instruction;
            //--------------------------------------------------------------
            instruction = new WriteCommand();                              //Створюю інструкцію WriteCommand
            chainInstructions.Add(instruction);                            //Додаю її в ланцюжок інструкцій операції BlockErase

            //    instruction.numberOfcycles = 0x01;                           //По дефолту 1       
            (instruction as WriteCommand).command = 0x60;                     //  Це поле заповнює користувач ( в конструкторі чіпа)
            (instruction as WriteCommand).Implementation = GetCommandMG;
            //--------------------------------------------------------------
            //--------------------------------------------------------------

            instruction = new WriteAddress();
            chainInstructions.Add(instruction);



            instruction.numberOfcycles = Chip.memOrg.rowAdrCycles; // /*(3Cycle)*/// Адрес буде підставляти кожний блок свій       
            (instruction as WriteAddress).Implementation = GetAddressMG;
            //--------------------------------------------------------------
            //--------------------------------------------------------------
            instruction = new WriteCommand();                              //Створюю інструкцію WriteCommand
            chainInstructions.Add(instruction);                            //Додаю її в ланцюжок інструкцій операції BlockErase

            //    instruction.numberOfcycles = 0x01;                           //По дефолту 1       
            (instruction as WriteCommand).command = 0xD0;
            (instruction as WriteCommand).Implementation = GetCommandMG;
            //--------------------------------------------------------------

        }
    }
}
